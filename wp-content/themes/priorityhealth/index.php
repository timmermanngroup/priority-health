<?php
/**
 * The main template file.
 *
 *
 * @package TG
 */
get_header(); 

?>
<main id="main" role="main">

<?php 
	if ( have_posts() ) {

		while( have_posts() ) {
			the_post();
		}
	}
?>

</main><!-- #main -->

<?php get_footer();