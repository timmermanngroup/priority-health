<?php

namespace ACF;

final class FieldLoader
{

    protected static $paths     =   [];


    /**
     * Add a Directory of files to be included
     * @access   public
     * @static
     * @param   string  $path
     * @param   int     $priority
     * 
     * @return  void
     */
    public static function addPath($path, $priority = 10)
    {
        self::$paths[$priority][] = $path;

        ksort(self::$paths, SORT_NUMERIC);
    }


    /**
     * Return an Array of paths to be included
     * @access   public
     * @static
     * 
     * @return  array
     */
    public static function getPaths()
    {

        return self::$paths;
    }


    /**
     * Include all registerd paths
     * @access  public
     * @static
     * @param   string  $file_type
     * 
     * @return  void
     */
    public static function includeFields($file_type = 'php')
    {
        $priorities  =   self::getPaths();

        if (
            !function_exists('acf_add_local_field_group')
            || empty($priorities)
        ) {
            return;
        }

        foreach($priorities as $index => $directories) {

            foreach ($directories as $directory) {
                if (!file_exists($directory)) {
                    continue;
                }

                $directory_iterator     =   new \DirectoryIterator($directory);

                foreach ($directory_iterator as $file_info) {

                    if (
                        !$file_info->isFile()
                        || $file_info->getExtension() != $file_type
                    ) {
                        continue;
                    }

                    $file_extension     =   strtoupper($file_info->getExtension());
                    $behavior           =   __NAMESPACE__."\FileInclude{$file_extension}";

                    if (
                        class_exists($behavior, true)
                        && is_subclass_of($behavior, __NAMESPACE__."\FileIncludeInterface")
                    ) {
                        $behavior::include($file_info);
                    }
                }
            }            
        }
    }
}