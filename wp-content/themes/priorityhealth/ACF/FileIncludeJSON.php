<?php

namespace ACF;

class FileIncludeJSON implements FileIncludeInterface
{


    /**
     * Parse the JSON into PHP
     * @access  private
     * @static
     * @param   string  $file_path
     * 
     * @return  array
     */
    private static function parseJson($file_path)
    {
        if (!file_exists($file_path)) {
            error_log("Path : $file_path does not exist.");
            return;
        }

        $file_contents  =   file_get_contents($file_path, true);

        return json_decode($file_contents, true)[0];
    }
    


    public static function include(\DirectoryIterator $file_info)
    {
        $path   =    $file_info->getPath().DIRECTORY_SEPARATOR.$file_info->getFilename();
        $array  =   static::parseJson($path);

        if (function_exists('acf_add_local_field_group')) {
            acf_add_local_field_group($array);
        }
    }
}