<?php

namespace ACF;

interface FileIncludeInterface
{

    /**
     * Include the given file
     * @access  public
     * @static
     * @param   \DirectoryIterator  $file_info
     * 
     * @return  void
     */
    public static function include(\DirectoryIterator $file_info);
}