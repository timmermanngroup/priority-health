<?php

namespace ACF;

class FileIncludePHP implements FileIncludeInterface
{

    /**
     * Include the given file
     * @access  public
     * @static
     * @param   \DirectoryIterator  $file_info
     * 
     * @return  void
     */
    public static function include(\DirectoryIterator $file_info)
    {
        $path   =    $file_info->getPath().DIRECTORY_SEPARATOR.$file_info->getFilename();
        
        include_once($path);
    }
}