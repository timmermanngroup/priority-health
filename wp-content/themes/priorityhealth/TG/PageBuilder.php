<?php

namespace TG;

use TG\Interfaces\HTMLElementStatic;
use TG\Admin\Fields;

abstract class PageBuilder implements HTMLElementStatic
{

    const LAYOUT_KEY    =   'acf_fc_layout';


    /**
     * Returns the Name of the Block Class
     * @access   private
     * @static
     * @return  string
     */
    private static function getBlockClass(array $block_data)
    {   
        if (!isset($block_data[self::LAYOUT_KEY])) { 
            return false; 
        }

        $class_name     =   Functions::convertToStudlyCase($block_data[self::LAYOUT_KEY]);
        $class_name     =   join('\\', [__NAMESPACE__, 'PageBuilderBlocks', $class_name]);

        if (
            !class_exists($class_name) 
         || !is_a($class_name, '\TG\PageBuilderBlocks\BlockAbstract', true)
        ) {
            $class_name     =   false;
        }

        return $class_name;
    }



    /**
     * Return the PageBuilder HTML
     * @access  public
     * @static
     * @return  string
     */
    public static function getHtml()
    {
        if (
            !is_page()
         && !function_exists('get_field')
         || !get_field(Fields::PAGE_BUILDER_BLOCKS)
        ) {
            return;
        }

        $content_areas  =   get_field(Fields::PAGE_BUILDER_BLOCKS);
        $html           =   '';
        foreach ($content_areas as $data) {

            $class  =   self::getBlockClass($data);
            if ($class) {
                $block  =   new $class($data);
                $html .= $block->getHtml();
            }
        }

        // echo '<pre>';
        // print_r($content_areas);
        // echo '</pre>';

        return apply_filters(__METHOD__, $html);
    }


    /**
     * Display the PageBuilder HTML
     * @access  public
     * @static
     * @return  void
     */
    public static function buildHtml()
    {
        print self::getHtml();
    }
}