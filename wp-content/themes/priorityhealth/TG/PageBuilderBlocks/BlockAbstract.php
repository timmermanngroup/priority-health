<?php

namespace TG\PageBuilderBlocks;

use TG\Interfaces\HTMLElement;

abstract class BlockAbstract implements HTMLElement
{


    abstract public function getHtml();


    public function buildHtml()
    {
        print $this->getHtml();
    }
}