<?php

namespace TG;

use \TG\Admin\PostTypeLoader;
use \TG\Admin\ScriptLoader;
use \TG\Admin\StyleLoader;
use \TG\Admin\TaxonomyLoader;

class Theme
{

    use Traits\Singleton;


    /**
     * Text Domain for the theme
     * @access  private
     * @static
	 * @var     string
     */
    private static $textdomin;


    /**
     * Theme Name
     * @access  private
     * @static
     * @var     string
     */
    private static $theme_name;


    /**
     * Return the textdomain
     * @access  public
     * @return  string
     */
    public function getTextdomain()
    {
        return self::$textdomin;
    }



    /**
     * Initialize the Theme Class
     * @access  public
     * @static
     * @return  void
     */
    public static function init()
    {

        self::setup();

        //Add Hooks and Filters
        add_action('init', ['TG\Filters', 'init']);
        add_action('init', ['TG\Actions', 'init']);
        add_action( 'wp_head', [__CLASS__, 'noindexNonProduction'], 0);

        add_action('init', [__CLASS__, 'addThemeSupport']);
        add_action('init', ['TG\Admin\ThemeOptions', 'registerNavMenus']);
        
        add_action('init', [__CLASS__, 'wpCleanUp']);
        add_action('init', [__CLASS__, 'registerPostTypes']);
        add_action('init', [__CLASS__, 'registerTaxonomies']);
        add_action('widgets_init', ['TG\Admin\Widgets', 'init']);
        
        add_action('wp_enqueue_scripts', [__CLASS__, 'enqueueScripts']);

        add_filter('admin_footer_text', [__CLASS__, 'updateFooterText']);
    }



    /**
     * Do the Theme Setup
     * @access  private
     * @static
     * @return  void
     */
    private static function setup()
    {
        $theme_data     =   wp_get_theme();
        self::$textdomin    =   $theme_data->get('TextDomain');
        self::$theme_name   =   $theme_data->get('Name');

        if (!defined("GA_DEBUG")) {
            define("GA_DEBUG", WP_DEBUG);
        }
    }


    /**
     * Establish Theme Support
     * @access  public
     * @static
     * @return  void
     */
    public static function addThemeSupport()
    {
        add_theme_support('automatic-feed-links');
        add_theme_support('title-tag');
        add_theme_support(
            'html',
            [
                'search-form',
                'gallery',
                'caption'
            ]
        );
        add_theme_support('post-thumbnails');

        do_action(__METHOD__);
    }


    /**
     * Clean Up all the un-needed thing WP does
     * @access  public
     * @static
     * @return  void
     */
    public static function wpCleanUp()
    {
        remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
        remove_action( 'wp_head', 'wp_generator' );
        
        //Remove Script and Style Versions
        add_filter('script_loader_src',[__CLASS__, 'removeScriptVersion'], 15, 2);
		add_filter('style_loader_src', [__CLASS__, 'removeScriptVersion'], 15, 2);
    }



    /**
     * Remove Versions from Scritps and Styles
     * @access  public
     * @static
     * @param   string  $src
     * @param   string  $handle
     * @return  string
     */
    public static function removeScriptVersion($src, $handle)
    {
        $parts          =   explode('?ver=', $src);

        return str_replace(['http:', 'https'], '', $parts[0]);
    }


    /**
     * Enqueue Scripts/Styles
     * @access  public
     * @static
     * @return  void
     */
    public static function enqueueScripts()
    {

        //Add Main CSS File
        StyleLoader::add(TG()->getTextdomain(), get_template_directory_uri() . '/css/style.css', [], null, 'screen');

        //Add Main JS file
        ScriptLoader::add(TG()->getTextdomain(), get_template_directory_uri() . '/js/main.js', ['jquery'], null, true);

        //Enqueue the styles
        ScriptLoader::init();
        StyleLoader::init();
    }


    /**
     * Register Post Types
     * @access  public
     * @static  
     * 
     * @see \TG\Admin\PostTypeLoader::add for parameters
     * 
     * @return  void
     */
    public static function registerPostTypes()
    {
        
        PostTypeLoader::register();
    }


    /**
     * Register Taxonomoies
     * @access  public
     * @static
     * 
     * @see \TG\Admin\TaxonomyLoader::add for parameters
     *
     * @return void
     */
    public static function registerTaxonomies()
    {

        TaxonomyLoader::register();
    }


    /**
	 * Give TG Credit for the website
	 * @access 	public
     * @static
	 * @return 	void
	 */
    public static function updateFooterText() 
    {
        return 'Site designed and developed by <a href="http://www.wearetg.com/" target="_blank">Timmermann Group</a> and powered by <a href="http://wordpress.org" target="_blank">WordPress</a>.';
    }



    /**
     * No Index if not Production
     * 
     */
    public static function noindexNonProduction()
    {
        $index  =   true;

        if (defined('WP_ENV')) {
            $index  =   strtolower(constant('WP_ENV')) != 'production' ? __return_false(): !constant('WP_DEBUG');
        }

        if ($index) {
            return;
        }

        wp_no_robots();
    }
}