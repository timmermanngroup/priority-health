<?php

namespace TG;

/**
 * Autoload Classes as needed
 * @param   string  $class_name
 * @return  void
 */
function autoloader($class_name)
{
    $class_name = ltrim($class_name, '\\');
    $vender_ns = strpos($class_name, __NAMESPACE__);

    if ($vender_ns !== false) {
        $file_name  = '';
        $namespace = '';
        if ($last_ns_pos = strrpos($class_name, '\\')) {
            $namespace = substr($class_name, 0, $last_ns_pos);
            $class_name = substr($class_name, $last_ns_pos + 1);
            $file_name  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
        }
        $file_name .= $class_name . '.php';

        $file_name  =   get_template_directory() . DIRECTORY_SEPARATOR . $file_name;

        if (file_exists($file_name)) {
            require_once($file_name);
        } 
    }
}

//register the autolaoder function
spl_autoload_register(__NAMESPACE__ . '\autoloader');