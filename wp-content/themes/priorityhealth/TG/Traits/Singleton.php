<?php

namespace TG\Traits;

trait Singleton 
{


    
    /**
     * Single Insatnce of the class
     * @access  protected
     * @static  
     * @var     null|object
     */
    protected static $instance  =   null;



    /**
     * Retunr the single instance of the class
     * @access  public 
     * @static
     * @return  object
     */
    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }
}