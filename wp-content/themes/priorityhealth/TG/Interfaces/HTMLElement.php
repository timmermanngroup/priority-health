<?php

namespace TG\Interfaces;

interface HTMLElement
{

    public function getHtml();

    public function buildHtml();
}