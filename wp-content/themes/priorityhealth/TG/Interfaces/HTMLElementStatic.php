<?php

namespace TG\Interfaces;

interface HTMLElementStatic
{

    public static function getHtml();

    public static function buildHtml();
}