<?php

namespace TG\Admin;

abstract class TaxonomyLoader
{


    private static $taxonomies = [];


    /**
     * Add a Taxonomy to be registered
     *
     * @param string    $handle
     * @param array     $post_types
     * @param array     $labels
     * @param array     $args
     *
     * @return void
     */
    public static function add(
        $handle,
        $post_types = [],
        $labels = [],
        $args = []
    ) {

        $handle             =   strtolower($handle);
        $taxonomy_name      =   ucwords(str_replace(['-', '_'], ' ', $handle));

        $labels     =   wp_parse_args($labels, [
            'name'                       => _x( "$taxonomy_name", 'Taxonomy General Name', TG()->getTextdomain() ),
            'singular_name'              => _x( "$taxonomy_name", 'Taxonomy Singular Name', TG()->getTextdomain() ),
            'menu_name'                  => __( "$taxonomy_name", TG()->getTextdomain() ),
            'all_items'                  => __( "All $taxonomy_name", TG()->getTextdomain() ),
            'parent_item'                => __( 'Parent Item', TG()->getTextdomain() ),
            'parent_item_colon'          => __( 'Parent Item:', TG()->getTextdomain() ),
            'new_item_name'              => __( "New $taxonomy_name", TG()->getTextdomain() ),
            'add_new_item'               => __( "Add New $taxonomy_name", TG()->getTextdomain() ),
            'edit_item'                  => __( 'Edit Item', TG()->getTextdomain() ),
            'update_item'                => __( 'Update Item', TG()->getTextdomain() ),
            'view_item'                  => __( 'View Item', TG()->getTextdomain() ),
            'separate_items_with_commas' => __( 'Separate items with commas', TG()->getTextdomain() ),
            'add_or_remove_items'        => __( 'Add or remove items', TG()->getTextdomain() ),
            'choose_from_most_used'      => __( 'Choose from the most used', TG()->getTextdomain() ),
            'popular_items'              => __( 'Popular Items', TG()->getTextdomain() ),
            'search_items'               => __( 'Search Items', TG()->getTextdomain() ),
            'not_found'                  => __( 'Not Found', TG()->getTextdomain() ),
            'no_terms'                   => __( 'No items', TG()->getTextdomain() ),
            'items_list'                 => __( 'Items list', TG()->getTextdomain() ),
            'items_list_navigation'      => __( 'Items list navigation', TG()->getTextdomain() ),
        ]);


        $args   =   wp_parse_args($args, [
            'hierarchical'               => true,
            'public'                     => true,
            'show_ui'                    => true,
            'show_admin_column'          => true,
            'show_in_nav_menus'          => false,
            'show_tagcloud'              => true,
        ]);

        $args['labels'] = $labels;

        self::$taxonomies[$handle]  =   [
            'post_types'    =>  $post_types,
            'args'          =>  $args
        ];
    }


    /**
     * Return the array of Taxonomies to be registered
     *
     * @return void
     */
    public static function getTaxonomies()
    {
        return apply_filters(__METHOD__, self::$taxonomies);
    }


    /**
     * Register all added Taxonomies
     */
    public static function register()
    {
        $taxonomies     =   self::getTaxonomies();

        if (empty($taxonomies)) {
            return;
        }

        foreach ($taxonomies as $handle => $taxonomy) {

            register_taxonomy( $handle, $taxonomy['post_types'], $taxonomy['args'] );
        }
    }
}