<?php

namespace TG\Admin;

abstract class StyleLoader
{
    private static $styles = [];


    public static function init()
    {
        self::register();
        self::enqueue();
    }



    /**
     * Add a style to be registered
     *
     * @param string $handle
     * @param string $src
     * @param array $deps
     * @param string|bool|null $ver
     * @param string $media
     *
     * @return void
     */
    public static function add( 
        $handle, 
        $src, 
        $deps = [], 
        $ver = null, 
        $media = 'screen'
    ) {

        self::$styles[$handle]     =   func_get_args();
    }



    /**
     * Reove a Regstered script
     *
     * @param string $handle
     *
     * @return void
     */
    public static function remove($handle)
    {
        $scripts    =   self::getStyles();

        if (array_key_exists($handle, $scripts)) {
            unset(self::$scripts[$handle]);
        }
    }



    /**
     * Return all Registered Scritps
     *
     * @return array
     */
    public static function getStyles()
    {
        return apply_filters(__METHOD__, self::$styles);
    }



    /**
     * Enqueu all registerd styles
     *
     * @return void
     */
    public static function enqueue()
    {
        $styles    =   self::getStyles();

        if (empty($styles)) {
            return;
        }

        foreach ($styles as $style) {
            call_user_func_array('wp_enqueue_style', $style);
        }
    }



    public static function register()
    {
        $styles    =   self::$styles;

        if (empty($styles)) {
            return;
        }

        foreach ($styles as $style) {
            call_user_func_array('wp_register_style', $style);
        }
    }
}