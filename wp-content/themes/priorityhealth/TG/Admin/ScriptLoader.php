<?php

namespace TG\Admin;

abstract class ScriptLoader
{
    private static $scripts    =   [];

    private static $localized  =   [];


    /**
     * Initilaze the Scripts 
     *
     * @return void
     */
    public static function init()
    {
        self::register();
        self::enqueue();
    }


    /**
     * Add a Script to the array of scripts to be registered
     * @access  public
     * @static
     * @param   string  $handle
     * @param   string  $src
     * @param   array   $deps
     * @param   string  $ver
     * @param   bool    $in_footer
     * 
     * @return  void
     */
    public static function add(
        $handle,
        $src,
        $deps = [],
        $ver = null,
        $in_footer = true
    ) {
        self::$scripts[$handle]     =   func_get_args();
    }



    /**
     * Remove a Script from the array of scripts to be registered
     * @access  public
     * @static
     * @param   string  $handle
     * 
     * @return  void
     */
    public static function remove($handle)
    {
        $scripts    =   self::$scripts;

        if (array_key_exists($handle, $scripts)) {
            unset($scripts[$handle]);
        }
    }



    /**
     * Return the array of added Scripts
     * @access  public
     * @static
     * 
     * @return  array
     */
    public static function getScripts()
    {
        return apply_filters(__METHOD__, self::$scripts);
    }


    /**
     * Register scripts
     *
     * @return void
     */
    private static function register()
    {
        $scripts    =   self::getScripts();

        if (!empty($scripts)) {

            foreach ($scripts as $handle => $args) {
                call_user_func_array('wp_register_script', $args);

                if (array_key_exists($handle, self::$localized)) {
                    call_user_func_array('wp_localize_script', self::$localized[$handle]);
                }
            }
        }
    }


    /**
     * Enqueue all registerd scripts
     *
     * @return void
     */
    public static function enqueue()
    {
        $scripts    =   self::getScripts();

        if (empty($scripts)) {
            return;
        }

        foreach ($scripts as $args) {
            call_user_func_array('wp_enqueue_script', $args);
        }
    }


    /**
     * Localize registered scripts
     *
     * @param string $handle
     * @param string $object_name
     * @param array $data
     *
     * @return void
     */
    public static function localize( $handle, $object_name, $data) 
    {
        $data   =   apply_filters( __METHOD__, $data, $handle);

        self::$localized[$handle]  =   [
            $handle,
            $object_name,
            $data
        ];
    }
}