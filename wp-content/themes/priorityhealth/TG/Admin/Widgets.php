<?php

namespace TG\Admin;

class Widgets 
{


    /**
     * Intialize the Widgets 
     * @access  public
     * @static
     * @return  void
     */
    public static function init()
    {
        register_sidebar([
            'name'          => esc_html__( 'Sidebar', TG()->getTextdomain() ),
			'id'            => 'sidebar-1',
			'description'   => '',
			'before_widget' => '<aside id="%1$s" class="widget %2$s">',
			'after_widget'  => '</aside>',
			'before_title'  => '<h4 class="widget-title">',
			'after_title'   => '</h4>',
        ]);
    }
}