<?php

namespace TG\Admin;

class ThemeOptions 
{

    /**
	 * Create Menus
	 * @uses register_nav_menus()
	 */
	public static function registerNavMenus() {

		register_nav_menus(
			array(
				'primary' => esc_html__( 'Primary Menu', TG()->getTextdomain() ),
			)
		);
	}
}