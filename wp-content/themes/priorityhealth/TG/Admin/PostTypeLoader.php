<?php

namespace TG\Admin;

class PostTypeLoader
{

    private static $post_types  =   [];



    /**
     * Add a New Post Type
     * 
     * @param   string  $handle
     * @param   array   $labels
     * @param   array   $args
     * 
     * @return void
     */
    public static function add(
        $handle,
        $labels = [],
        $args = []
    ) {
        $handle             =   strtolower($handle);
        $post_type_name     =   ucwords(str_replace(['-', '_'], ' ', $handle));

        $labels = wp_parse_args( $labels, [
            'name'                  => _x( $post_type_name, "$post_type_name General Name", TG()->getTextdomain() ),
            'singular_name'         => _x( $post_type_name, "$post_type_name Singular Name", TG()->getTextdomain() ),
            'menu_name'             => __( $post_type_name, TG()->getTextdomain() ),
            'name_admin_bar'        => __( $post_type_name, TG()->getTextdomain() ),
            'archives'              => __( "$post_type_name Archives", TG()->getTextdomain() ),
            'attributes'            => __( 'Item Attributes', TG()->getTextdomain() ),
            'parent_item_colon'     => __( 'Parent Item:', TG()->getTextdomain() ),
            'all_items'             => __( 'All Items', TG()->getTextdomain() ),
            'add_new_item'          => __( "Add New $post_type_name", TG()->getTextdomain() ),
            'add_new'               => __( 'Add New', TG()->getTextdomain() ),
            'new_item'              => __( 'New Item', TG()->getTextdomain() ),
            'edit_item'             => __( 'Edit Item', TG()->getTextdomain() ),
            'update_item'           => __( 'Update Item', TG()->getTextdomain() ),
            'view_item'             => __( 'View Item', TG()->getTextdomain() ),
            'view_items'            => __( 'View Items', TG()->getTextdomain() ),
            'search_items'          => __( 'Search Item', TG()->getTextdomain() ),
            'not_found'             => __( 'Not found', TG()->getTextdomain() ),
            'not_found_in_trash'    => __( 'Not found in Trash', TG()->getTextdomain() ),
            'featured_image'        => __( 'Featured Image', TG()->getTextdomain() ),
            'set_featured_image'    => __( 'Set featured image', TG()->getTextdomain() ),
            'remove_featured_image' => __( 'Remove featured image', TG()->getTextdomain() ),
            'use_featured_image'    => __( 'Use as featured image', TG()->getTextdomain() ),
            'insert_into_item'      => __( 'Insert into item', TG()->getTextdomain() ),
            'uploaded_to_this_item' => __( 'Uploaded to this item', TG()->getTextdomain() ),
            'items_list'            => __( 'Items list', TG()->getTextdomain() ),
            'items_list_navigation' => __( 'Items list navigation', TG()->getTextdomain() ),
            'filter_items_list'     => __( 'Filter items list', TG()->getTextdomain() ),
        ]);


        $args = wp_parse_args( $args, [
            'label'                 => __( $labels['name'], TG()->getTextdomain() ),
            'description'           => __( "{$labels['name']} Description", TG()->getTextdomain() ),
            'supports'              => [
                'title',
                'editor',
                'permalink',
                'thumbnail'
            ],
            'taxonomies'            => [],
            'hierarchical'          => false,
            'public'                => true,
            'show_ui'               => true,
            'show_in_menu'          => true,
            'menu_position'         => 5,
            'show_in_admin_bar'     => true,
            'show_in_nav_menus'     => true,
            'can_export'            => true,
            'has_archive'           => true,
            'exclude_from_search'   => false,
            'publicly_queryable'    => true,
            'rewrite'               => [
                'with_front'    =>  false
            ],
            'capability_type'       => 'page',
        ]);

        $args['labels']     =   $labels;

        self::$post_types[$handle]  =   apply_filters(__METHOD__, $args, $handle);
    }



    /**
     * Return the Array of Post Types
     * 
     * @return  array
     */
    public static function getPostTypes()
    {
        return apply_filters(__METHOD__, self::$post_types );
    }


    /**
     * Register the added Post Types
     * 
     * @return  void
     */
    public static function register()
    {
        $post_types     =   self::getPostTypes();

        if (!empty($post_types)) {

            foreach ($post_types as $handle => $args) {
                register_post_type( $handle, $args );
            }
        }
    }
}