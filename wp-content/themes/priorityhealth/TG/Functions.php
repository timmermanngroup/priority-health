<?php

namespace TG;

abstract class Functions
{


    /**
     * Convert String to StudlyCase
     * @access  public
     * @static
     * @return  string
     */
    public static function convertToStudlyCase($string)
    {
        $studly_case     =   str_replace(['_', '-'], ' ', $string);
        $studly_case     =   str_replace(' ', '', ucwords($studly_case));

        return apply_filters(__METHOD__, $studly_case, $string);
    }


    /**
     * Clean Numbers for tel: links
     * @access  public
     * @static 
     * @param 	string|number   $number
     * @param 	bool            $echo
     * @return 	string
     */
    public static function sanatizePhoneNumber( $number, $echo = true ) 
    {
        $no_spaces  =   preg_replace("/[^a-z0-9]/i", "", $number);
        $letters2   =   preg_replace("/[a-c]/i", "2", $no_spaces);
        $letters3   =   preg_replace("/[d-f]/i", '3', $letters2);
        $letters4   =   preg_replace("/[g-i]/i", '4', $letters3);
        $letters5   =   preg_replace("/[j-l]/i", '5', $letters4);
        $letters6   =   preg_replace("/[m-o]/i", '6', $letters5);
        $letters7   =   preg_replace("/[p-s]/i", '7', $letters6);
        $letters8   =   preg_replace("/[t-v]/i", '8', $letters7);
        $letters9   =   preg_replace("/[w-z]/i", '9', $letters8);

        if( !$echo ) {
            return $letters9;

        } else {
            echo $letters9;
        }
    }



    /**
     * Create attributes from array
     * @access  public
     * @static 
     * @param   array       $attributes
     * @param   string      $callback
     */
    public static function createHtmlAttributes( $attributes, $callback = 'esc_attr' ) 
    {
        if( empty( $attributes ) ) {
            return;
        }

        $attribute_array   =   [];

        foreach( $attributes as $key => $value ) {
            $attribute_array[]  =   $callback ? "$key=\"{$callback( $value )}\"": "$key=\"{$value}\"";
        }

        return join( ' ', $attribute_array );
    }



    /**
     * Format a link
     * @access  public
     * @static
     * @param   array           $data
     * @param   array|string    $attrs      //Array of attributes $key => $value
     * @return  string
     */
    public static function getLink( $data = [], $attrs = [] ) 
    {
        if( empty( $data['url'] ) && empty( $data['title'] ) ) {
            return;
        }

        $attributes     =   [
            'href'      =>  $data['url'],
            'target'    =>  !empty( $data['target'] ) ? $data['target']: '',
            'title'     =>  __( $data['title'], TG()->textdomain )
        ];

        if( is_string( $attrs ) ) {
            $attributes['class']    =   $attrs;

        } elseif( is_array( $attrs ) ) {
            $attributes     =   array_merge( $attributes, $attrs );
        }

        $button     =   sprintf(
            '<a %2$s>%1$s</a>',
            $data['title'],
            self::create_attributes( $attributes )
        );

        return apply_filters( __METHOD__, $button );
    }
}