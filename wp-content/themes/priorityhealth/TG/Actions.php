<?php

namespace TG;

abstract class Actions
{

    const BODY_START        =   'tg\body\start';
    const FOOTER_START      =   'tg\footer\start';


    /**
     * Initilize Actions
     * @access  public
     * @static
     * @return  void
     */
    public static function init()
    {

    }
}