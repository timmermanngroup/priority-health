const gulp = require('gulp');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const sourcemaps = require('gulp-sourcemaps');
const cleanCSS = require('gulp-clean-css');

const concat = require('gulp-concat');
const deporder = require('gulp-deporder');
const stripdebug = require('gulp-strip-debug');
const babel = require('gulp-babel');
const uglify = require('gulp-uglify');


sass.compiler = require('node-sass');


/**
 * Compile, autoprefix, minify, and add source maps to SCSS files
 */
gulp.task('sass', function() {
    let options = {
        file : '.src/scss/style.scss',
        outFile : './css/style.css',
        outputStyle : 'nested',
        errLogToConsole : true,
        includePaths : ['./node_modules']
    };
    return gulp.src('./src/scss/**/*.scss')
        .pipe(sourcemaps.init())
            .pipe(autoprefixer({
                browsers : ['last 2 versions'],
                cascade : true
            }))
            .pipe(sass(options))
        .pipe(cleanCSS())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('./css'));
});


/**
 * Compile all JS files
 */
gulp.task('js', function() {

    return gulp.src('./src/js/**/*.js')
        .pipe(sourcemaps.init())
            .pipe(deporder())
            .pipe(concat('main.js'))
            // .pipe(stripdebug())
            .pipe(babel({
                presets: ['@babel/env']
            }))
            .pipe(uglify())
        .pipe(gulp.dest('./js'));
})


//Define the Watch Task
gulp.task('watch', function() {
    gulp.watch('./src/**/*.scss', gulp.task('sass'));
    gulp.watch('./src/**/*.js', gulp.task('js'));
});
