<?php
/**
 * The template for displaying the footer.
 *
 * @package TG
 */

do_action(\TG\Actions::FOOTER_START);
wp_footer(); ?>
</body>
</html>