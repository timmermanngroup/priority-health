<?php

//Require the autoloader
require dirname(__FILE__) . '/TG/autoloader.php';
require dirname(__FILE__) . '/ACF/autoloader.php';

//Return the instance of the \TG\Theme
function TG() {
    return \TG\Theme::getInstance();
}

function include_acf_fields() {

    if (!function_exists('acf_add_local_field_group')) {
        return;
    }

    ACF\FieldLoader::includeFields();
}


//Include all ACF Fields
add_action( 'after_setup_theme', 'include_acf_fields');

//Initialize the Theme
add_action('after_setup_theme', ['TG\Theme', 'init']);


